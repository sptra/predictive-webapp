
var dbm = require('db-migrate');
var type = dbm.dataType;
var async = require('async');

exports.up = function(db, callback) {
  async.series([
    db.createTable.bind(db, 'blogs', {
      id: { type: 'int', primaryKey: true, autoIncrement: true },
      created: { type: 'datetime', notNull: true },
      last_updated: { type: 'datetime', notNull: true },
      category_id: { type: 'int', notNull: true },
      title: { type: 'string' },
      body: { type: 'text' }
    }),

    db.createTable.bind(db, 'categories', {
      id: { type: 'int', primaryKey: true, autoIncrement: true },
      name: { type: 'string' }
    }),

    db.createTable.bind(db, 'keywords', {
      id: { type: 'int', primaryKey: true, autoIncrement: true },
      name: { type: 'string' }
    }),

    db.createTable.bind(db, 'blogs_keywords', {
      blog_id: { type: 'int', primaryKey: true },
      keyword_id: { type: 'int', primaryKey: true }
    }),
    
    db.createTable.bind(db, 'taxis', {
      id: { type: 'int', primaryKey: true, autoIncrement: true },
      name: { type: 'string' }
    }),
    
    db.createTable.bind(db, 'bookings', {
      id: { type: 'int', primaryKey: true, autoIncrement: true },
      name: { type: 'string' }
    }),
    
    db.createTable.bind(db, 'histories', {
      id: { type: 'int', primaryKey: true, autoIncrement: true },
      created: { type: 'datetime', notNull: true },
      last_updated: { type: 'datetime', notNull: true },
      pickup_area: { type: 'string' },
      taxi_id: { type: 'int', notNull: true },
      booking_id: { type: 'int', notNull: true }
    })
  ], callback);
};

exports.down = function(db, callback) {
  async.series([
    db.dropTable.bind(db, 'blogs'),
    db.dropTable.bind(db, 'categories'),
    db.dropTable.bind(db, 'keywords'),
    db.dropTable.bind(db, 'blogs_keywords'),
    db.dropTable.bind(db, 'taxis'),
    db.dropTable.bind(db, 'bookings'),
    db.dropTable.bind(db, 'histories')
  ], callback);
};
