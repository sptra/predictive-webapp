
var persist = require("persist");
var Taxi = require("./taxi");
var Booking = require("./booking");
var type = persist.type;

module.exports = History = persist.define("History", {
  "created": { type: type.DATETIME, defaultValue: function() { return new Date() } },
  "lastUpdated": { type: type.DATETIME },
  "pickupArea": { type: type.STRING }
})
  .hasOne(Taxi)
  .hasOne(Booking);

History.onSave = function(obj, connection, callback) {
  obj.lastUpdated = new Date();
  callback();
}
